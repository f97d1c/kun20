#!/bin/bash --login

echo '安装ruby基本环境依赖'
apt install -fy gnupg2 curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev nodejs

# echo '安装Mysql数据库相关软件'
# # 这个必须要安装,不然镜像会大300多MB
# apt install -fy mysql-client libmysqlclient-dev