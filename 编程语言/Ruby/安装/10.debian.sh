#!/bin/bash --login
eval "export $(jq -r '.["shell变量"]' $1)"

# 代理的变量不能存在引号及分号
# eval "export https_proxy=$(jq -r '."Http代理"' $1) http_proxy=$(jq -r '."Https代理"' $1) all_proxy=$(jq -r '."所有代理"' $1)"

if [ "$(command -v rvm)" ]; then
  if [ "$(rvm list | grep $ruby_version)" ]; then
    echo "Ruby:$ruby_version 已安装, 无需再次安装."
    rvm list
    echo '[true, ""]'
    exit 0
  fi
fi

sudo apt update
sudo bash ./编程语言/Ruby/安装/02.功能级依赖.debian.sh

if ! [ "$(command -v rvm)" ]; then
  echo '安装RVM'

  # curl -SL https://rvm.io/mpapis.asc | sudo gpg --import -
  # curl -SL https://rvm.io/pkuczynski.asc | sudo gpg --import -
  curl -SL https://rvm.io/mpapis.asc | sudo gpg2 --import -
  curl -SL https://rvm.io/pkuczynski.asc | sudo gpg2 --import -

  curl -SL https://get.rvm.io | sudo bash stable
  # source ~/.rvm/scripts/rvm
  source /usr/local/rvm/scripts/rvm

  echo '设置RVM最大超时时间'
  sudo chown $(id -u -n) ~/.rvmrc
  echo "export rvm_max_time_flag=20" >>~/.rvmrc
  source ~/.rvmrc

  echo '安装RVM相关依赖'
  rvm requirements

  # echo '修改更新源'
  # echo "ruby_url=https://cache.ruby-china.org/pub/ruby" >> ~/.rvm/user/db

  # '解决: You need to change your terminal emulator preferences to allow login shell.'
  echo '[[ -s "/home/$(id -u -n)/.rvm/scripts/rvm" ]] && . "/home/$(id -u -n)/.rvm/scripts/rvm"' >> ~/.bashrc
  source ~/.bashrc
fi

sudo chown -R $(id -u -n) /usr/local/rvm
echo "安装Ruby($ruby_version)"
rvm install $ruby_version

# 提示没有rvm命令 暂时注释
# ubuntu server set run command as a login shell
# echo "指定Ruby默认版本($ruby_version)"
# rvm use $ruby_version --default

echo '设置Gem更新源'
gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/

echo '安装Bundler'
# 注意版本 已知1.16.2版本会引发: Traceback (most recent call last)
# 可安装指定版本: gem install bundler 1.16.6
gem install bundler

echo '[true, ""]'