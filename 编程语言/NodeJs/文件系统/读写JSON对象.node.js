fs = require('fs')

class JsonRW {
  constructor(params = {}) {
    let defaultValue = {
      文件绝对路径: undefined,
      文件内容: {}
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

    let data
    if (fs.existsSync(this.文件绝对路径)) {
      data = fs.readFileSync(this.文件绝对路径, 'utf8')
    } else {
      data = '{}'
      fs.writeFileSync(this.文件绝对路径, data);
    }

    try {
      this.文件内容 = JSON.parse(data)
    } catch (error) {
      // this.文件内容 = {}
      console.log(JSON.stringify([false, '[读写JSON对象]JSON对象解析失败', { 异常信息: error.toString(), 文件绝对路径: this.文件绝对路径 }]))
      process.exit(255)
    }
  }

  read() {
    return this.文件内容
  }

  add(key = null, value = null) {
    // let tmp = {}
    // tmp[key] = value
    // this.文件内容 = Object.assign(this.文件内容, tmp)
    this.文件内容[key] = value
    this.update()
  }

  update(result = this.文件内容) {
    fs.writeFileSync(this.文件绝对路径, JSON.stringify(result, null, 2))
  }

}

module.exports = JsonRW;
