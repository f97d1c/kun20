class JShell {
  constructor(params = {}) {
    let defaultValue = {
      打印输出: false,
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

  }
}

module.exports = JShell;
