/**
 * json_command 用于将json格式的数据转换为Shell命令.
 * 例如:
 * {
 *   "apt": {
 *     "update": "",
 *     "install -fy": [
 *       "apt-utils",
 *       "sudo"
 *     ]
 *   }
 * }
 * 转化为:
 * apt update
 * apt install -fy apt-utils sudo
 * 
 */

const JShell = require("./core.node.js")

class JsonCommand extends JShell {
  constructor(params = {}) {
    super(params)
  }

  to_command(object) {
    let result = []
    let set_result = (items) => {
      let str = items.flat().join(' ')
      if (this.打印输出){ console.log(str) }
      result.push(str)
    }

    let iteration = (object, items=[]) => {
      switch (typeof(object)){
        case 'object':
          if (Array.isArray(object)){
            set_result([items, object])
            break
          }

          if (object == null){
            set_result([items])
            break
          }

          for (let [key, value] of Object.entries(object)) {
            iteration(value, [items, key].flat())
          }
          break;
        case 'string':
          set_result([items, object])
          break;
        default:
          break;
      }

    }

    for (let [key, value] of Object.entries(object)) {
      iteration(value, [key])
    }
    return result
  }

}

module.exports = JsonCommand;
