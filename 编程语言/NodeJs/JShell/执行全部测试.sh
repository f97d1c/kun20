#!/bin/bash --login

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../.."
fi

cd "$kun_main_path/应用/JShell"

find test/*/*.js -type f -print0 | while IFS= read -r -d $'\0' file; do
  echo "开始执行: $file"
  node $file
  if [[ ! $? -eq 0 ]];then break;fi
done