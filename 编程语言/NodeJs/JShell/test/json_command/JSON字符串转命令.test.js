const JsonCommand = require("../../core/json_command.node")

jcommand = new JsonCommand({ 打印输出: false })

params = {
  "curl": "-s -L https://gitlab.com/f97d1c/kun/-/raw/master/packages/install_latest.sh | bash",
  "sudo": {
    "kun": {
      "system": {
        "初始化": {
          "改为国内源": "0 y"
        },
        "debian": {
          "更新系统": null
        }
      }
    },
    "apt": {
      "update": null,
      "install -fy": [
        "apt-utils",
        "sudo",
        "lsb-release",
        "ca-certificates",
        "gawk",
        "curl",
        "vim",
        "git",
        "openssh-server",
        "tmux",
        "htop",
        "net-tools",
        "cron",
        "unzip"
      ]
    }
  },
  "kun": {
    "system": {
      "初始化": {
        "本地时间": null
      },
      "bashrc": {
        "标准化bashrc": null
      },
      "app": {
        "tmux": "初始化"
      },
      "debian": {
        "清理缓存": null
      }
    }
  }
}

let result = jcommand.to_command(params)
console.log(result)