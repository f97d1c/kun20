#!/bin/bash --login

# [NodeSource Node.js Binary Distributions](https://github.com/nodesource/distributions)

if [[ "$(command -v npm)" ]] && [[ $(node -v | sed -E "s/v([0-9]+).*/\1/g") -ge 13 ]];then
  npm install
  exit 0
fi

echo '安装nodejs'
apt-get purge -y nodejs npm

# curl -fsSL https://deb.nodesource.com/setup_17.x | sudo -E bash -
curl -fsSL https://deb.nodesource.com/setup_17.x | bash
apt-get install gcc g++ make
apt-get install -y nodejs

echo '设置npm国内更新源'
npm config set registry=https://registry.npmmirror.com

echo '安装cnpm'
npm install -g cnpm --registry=https://registry.npmmirror.com

npm install

if [[ "$(command -v npm)" ]] && [[ $(node -v | sed -E "s/v([0-9]+).*/\1/g") -ge 13 ]];then exit 0;fi