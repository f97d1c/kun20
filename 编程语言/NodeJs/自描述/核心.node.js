const fs = require("fs");
const execSync = require('child_process').execSync;
class Described {
  static initialize(params = {}) {
    return new Described(params)
  }

  constructor(params = {}) {

    let defaultValue = {
      描述: undefined,
      超管运行: false,
      运行环境: [],
      参数: {},
      shell变量: '',
      SHELL变量: '',
    }

    params = Object.assign(defaultValue, params)
    params.参数 = Object.assign({
      // 默认参数添加位置
    }, params.参数)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }

  }

  // 判断值原始类型
  // 基础typeof在类型表达上过于模糊例如3和3.14同属于number, 利用正则解析字符串进行相对准确判断
  _typeof(value) {
    if (!!!value) return typeof value;
    // if (typeof value == 'string') return 'string' // 字符串类型

    let valueStr = value.toString()
    if (typeof value == 'object') {
      if (Array.isArray(value)) { // 数组对象类型
        return 'array'
      } else if (!!(value.__proto__.toString().match(/^\[object .*Element\]$/))) { // html元素类型
        return 'element'
      } else {
        return '键值对' // 键值对类型
      }
    } else if (valueStr.match(/^\/((\/|(\\?))[\w .]+)+/i) && fs.existsSync(valueStr)) {
      return '绝对路径'
    } else if (valueStr.match(/^http(s|)\:\/\//)) {
      return '链接'
    } else if (!!valueStr.match(/^(\-|)\d{1,}$/)) {
      return '整数'
    } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{0,}$/)) {
      let num_length = valueStr.toString().match(/^(\-|)\d{1,}\.{1}\d{0,}$/)[0].split('.')[1].split('').length
      return '整数.'+num_length
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}$/)) {
      return '年-月-日'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}/)) {
      return '年-月-日T时:分:秒'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}\s\d{2}:\d{2}:\d{2}/)) {
      return '年-月-日 时:分:秒'
    } else { // 其他按原始类型
      return typeof value
    }
  }

  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
  }

  // 根据给定对象属性 转换为html的element元素
  toElement(params = {}) {
    let defaultValue = {
      element: undefined,
      innerHtml: undefined,
      outerText: undefined,
    }

    let object = Object.keys(params).filter(key => { return !Object.keys(defaultValue).includes(key) })
    params = Object.assign(defaultValue, params)

    let element = document.createElement(params.element)
    for (let key of object) {
      let value = params[key]
      if (typeof value == 'object') value = JSON.stringify(value)
      element.setAttribute(key, value)
    }

    // 判断元素内容是否为空不能用!!,0等于假 0 == false
    if (![null, undefined, ''].includes(params.innerHtml)) {
      if (typeof params.innerHtml != 'object') {
        element.innerHTML = params.innerHtml
      } else if (!Array.isArray(params.innerHtml)) {
        let res = this.toElement(params.innerHtml)
        if (res[0]) element.innerHTML = res[1].outerHTML
      } else {
        params.innerHtml.forEach(item => {
          if (typeof item != 'object') {
            element.innerHTML = item
          } else if (!Array.isArray(item)) {
            element.appendChild(this.toElement(item)[1])
          }
        })
      }
    }
    if (!!params.outerText) element = { outerHTML: (element.outerHTML + params.outerText) }
    return [true, element]
  }
  columns() {
    return Object.keys(this.参数)
  }

  validate(params) {
    if (this.超管运行) {
      if (!process.geteuid() == 0) return [false, '需使用超管权限运行'];
    }

    let res = this.validate_empty(params)
    if (!res[0]) return res

    res = this.validate_type(params)
    if (!res[0]) return res

    return [true, '']
  }

  validate_empty(params = {}) {
    // if (!!!this.描述) return [false, "描述信息中[描述]参数不能为空"]

    for (let [key, value] of Object.entries(this.参数)) {
      if ((params[key] == undefined) && (value.默认值 != undefined)) {
        this[key] = value.默认值
      } else if ((Array.isArray(params[key])) && (params[key].length == 0) && value.默认值 != undefined) {
        this[key] = value.默认值
      } else if ((params[key] == undefined) && value.默认值 == undefined) {
        let template = key + '为必填项, 不能为空'
        if (!!value.描述) template += ', 用于: ' + value.描述
        template += '.'
        return [false, template]
      } else if ((Array.isArray(params[key])) && (params[key].length == 0) && value.默认值 == undefined) {
        let template = key + '为必填项, 不能为空'
        if (!!value.描述) template += ', 用于: ' + value.描述
        template += '.'
        return [false, template]
      } else {
        this[key] = params[key]
      }

      if (value.提前加载 && this[key].toString().match(/.*\$\(.*\).*/)) {
        this[key] = execSync("echo " + this[key], { encoding: 'utf8' }).replace(/\n$/, "");
        // let shell_value
        // try {
        //   shell_value = execSync("echo " + this[key], { encoding: 'utf8' }).replace(/\n$/,"");
        // } catch (error) {
        //   shell_value = this[key]
        // }finally{
        //   this[key] = shell_value
        // }
      }

      if (value.变量名) {
        this[value.变量名] = this[key]
        this.shell变量 += value.变量名 + '=' + this[key] + '; '
        this.SHELL变量 += value.变量名.toUpperCase() + '=$' + value.变量名 + '; '
      }

    }
    return [true, '']
  }

  validate_type(params = {}) {
    for (let [key, value] of Object.entries(this.参数)) {
      let current_type = this._typeof(this[key])
      let require_types = [value.类型].flat()
      if (!require_types.includes(current_type)) {
        let template = key + ' 类型不符,要求类型为: ' + require_types.join('/') + ', 传递参数类型为: ' + current_type + '.'
        return [false, template, { '当前参数值': this[key] }]
      }
    }
    return [true, '']
  }

  createEle(params) {
    let elementInfo = {}
    Object.keys(params).filter(key => { return !this.columns().includes(key) }).forEach(item => {
      elementInfo[item] = params[item]
    })

    let res = this.toElement(Object.assign({
      element: 'div',
      id: this.元素ID
    }, elementInfo))

    if (!res[0]) return res
    this.element = res[1]
    this.insertEle(this.element)

    return [true, '']
  }

  insertEle(ele) {
    let childEle = document.getElementById(ele.id || this.uuid())
    if (!!childEle) {
      childEle.remove()
    }

    if (this.插入模式 == '后置') {
      this.父元素.append(ele)
    } else if (this.插入模式 == '前置') {
      this.父元素.prepend(ele)
    } else {
      this.插入模式(ele)
    }
  }

}

module.exports = Described;
