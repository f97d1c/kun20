#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

# 代理的变量不能存在引号及分号
# eval "export https_proxy=$(jq -r '."Http代理"' <<<"$res") http_proxy=$(jq -r '."Https代理"' <<<"$res") all_proxy=$(jq -r '."所有代理"' <<<"$res")"

echo "安装pyenv"
if [ ! -d "$pyenv_install_path" ]; then
  git clone https://github.com/pyenv/pyenv.git $pyenv_install_path
else
  echo "[跳过] $pyenv_install_path路径已存在"
fi

echo "安装pyenv-virtualenv"
if [ ! -d "$pyenv_install_path/plugins/pyenv-virtualenv" ]; then
  cd $pyenv_install_path/plugins
  git clone https://github.com/pyenv/pyenv-virtualenv.git
else
  echo "[跳过] $pyenv_install_path/plugins/pyenv-virtualenv路径已存在"
fi

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
eval "$(pyenv init --path)"

echo '写入pyenv环境变量'
pyenv_env=$(cat /home/$(id -u -n)/.bashrc | grep "pyenv相关内容")
if [ -n "$pyenv_env" ]; then
  echo "[跳过] 已写入相关环境变量"
else

sudo tee -a /home/$(id -u -n)/.bashrc <<-'EOF'

# === pyenv相关内容开始 ===
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/root/.pyenv/shims:${PATH}"
EOF

  echo -e "$(pyenv init --path)\n# === pyenv相关内容结束 ===" >>/home/$(id -u -n)/.bashrc
fi

echo "安装Python: $python_version"
if [ ! -d "$pyenv_install_path/versions/$python_version" ]; then
  pyenv install $python_version
  if [[ ! $? -eq 0 ]]; then
    echo '[false, "Python安装失败"]'
    exit 255
  fi
else
  echo "[跳过] 已安装$python_version版本"
fi

echo "创建Python虚拟环境: $virtualenv_name"
if [ -n "$(pyenv virtualenvs | grep $virtualenv_name)" ]; then
  echo "[跳过] 已存在同名虚拟环境"
else
  pyenv virtualenv $python_version $virtualenv_name
  if [[ ! $? -eq 0 ]]; then
    echo '[false, "虚拟环境构建失败"]'
    exit 255
  fi
fi

if [ ! "$(pyenv virtualenvs | grep $virtualenv_name)" ]; then
  echo '[false, "虚拟环境构建不符预期"]'
  exit 255
else
  echo '当前虚拟环境列表:'
  pyenv virtualenvs
fi

echo "切换Python虚拟环境"
pyenv activate $virtualenv_name
if [[ ! $? -eq 0 ]]; then
  echo '[false, "虚拟环境切换失败"]'
  exit 255
fi

current_python_version=$(python -c 'import sys; version=sys.version_info[:3]; print("{0}.{1}.{2}".format(*version))')
if [ ! "$python_version" == "$current_python_version" ]; then
  echo "当前默认Python版本: $current_python_version"
  echo '[false, "Python版本不符预期"]'
  exit 255
fi

echo '写入pyenv默认启动虚拟环境'
pyenv_env=$(cat /home/$(id -u -n)/.bashrc | grep "pyenv默认启动虚拟环境")
if [ -n "$pyenv_env" ]; then
  echo "[跳过] 已写入pyenv默认启动虚拟环境"
else
  echo -e "
# === pyenv默认启动虚拟环境开始 ===
pyenv activate $virtualenv_name
# === pyenv默认启动虚拟环境结束 ===" >>/home/$(id -u -n)/.bashrc
fi

echo "pip更新"
python -m pip install --upgrade pip

pip_python_version=$(pip -V | sed -E 's/.*versions\/python\-([0-9]\.[0-9]\.[0-9])\/lib.*/\1/g')
if [ ! "$python_version" == "$current_python_version" ]; then
  echo '[false, "pip使用版本与当前Python版本不符"]'
  exit 255
fi

echo '[true, ""]'
