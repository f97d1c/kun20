let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;

const execSync = require('child_process').execSync;
let dirs = JSON.parse(execSync("tree -u -g -sh -J " + argv1.目标路径, { encoding: 'utf8' }))[0]

let mktemp = require('mktemp');
let result_path = mktemp.createFileSync('./Linux/文件系统/文件索引/-XXXXXXX.json');
// let result_path = mktemp.createFileSync('/tmp/k20_XXXXXXX.json');

const md5File = require('md5-file')

const mime = require('mime');

result = {
  "基础": [],
  "md5sum": undefined,
  "后缀": {},
  "类型": {},
  "重复项": undefined,
  "去重项": undefined,
}

if (argv1.md5sum) {
  result['md5sum'] = {}
  result['重复项'] = {}
  result['去重项'] = []
}

let set_result = (file, items = []) => {
  let path = [argv1.目标路径, items, file.name].flat().join('/')
  let stat = fs.statSync(path)
  let suffix = file.name.split('.').at(-1)
  let type = mime.getType(path).split('/')
  base = {
    "文件名": file.name,
    "路径": path,
    "后缀": suffix,
    "类型": type,
    "大小": file.size,
    "大小(KB)": stat.size,
    "所属用户": file.user,
    "所属组": file.group,
    "md5sum": undefined,
    "创建时间": stat.birthtime,
    "修改时间": stat.mtime,
    "访问时间": stat.atime,
    "更改时间": stat.ctime,
  }

  if (argv1.md5sum) {
    base.md5sum = md5File.sync(path)
  }

  // atime: 自 POSIX 纪元以来最后一次访问此文件的时间，以毫秒为单位
  // mtime: 上次修改这个文件的时间...
  // ctime: 上次更改文件状态的时间...
  // birthtime: 这个文件的创建时间

  result['基础'].push(base)

  result['后缀'][suffix] ||= []
  result['后缀'][suffix].push(base)

  result['类型'][type[0]] ||= []
  result['类型'][type[0]].push(base)

  if (base.md5sum) {
    let exists_item = result['md5sum'][base.md5sum]
    if (exists_item) {
      result['重复项'][base.md5sum] ||= []
      result['重复项'][base.md5sum].push(base)
    } else {
      result['md5sum'][base.md5sum] = base
      result['去重项'].push(base)
    }
  }

}

let iteration = (content, items = []) => {
  if (content.type == 'file') {
    set_result(content, items)
    return
  }

  if (content.type == 'directory') {
    for (content_ of content.contents) {
      if (content.type == 'file') {
        set_result(content, items)
        return
      }

      if (argv1.忽略文件夹.includes(content_.name)) continue;
      iteration(content_, [items, content.name].flat())
    }
  }

}

for (content of dirs.contents) {
  if (content.type == 'file') {
    set_result(content, [])
    continue
  }
  if (argv1.忽略文件夹.includes(content.name)) continue;
  iteration(content)
}

let file_indexs = new JsonRW({ 文件绝对路径: result_path })
file_indexs.update(result)

console.log(JSON.stringify([true, result_path]))