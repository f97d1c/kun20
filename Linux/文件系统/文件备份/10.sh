#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

if [ ! -f "$backup_file" ]; then
  if [[ "$must_backup" == 'true' ]];then
    echo '[false, "目标文件不存在: '$backup_file'"]'
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
  else
    echo '[true, ""]'
    exit 0
  fi
fi

backup_file_md5_value=$(cat $backup_file | md5sum)
backup_file_md5_value=${backup_file_md5_value:0:32}
backup_file_name=${backup_file##*/}
backup_dir_name=${backup_file%/*}
backup_path="${backup_dir_name}/${backup_file_name%.*}.$(date '+%Y-%m-%d-%H_%M_%S')-$backup_file_md5_value"

# 如果文件后缀不为空则添加后缀
if [[ ! "${backup_file_name##*.}" == "${backup_file_name}" ]];then
  backup_path+=".${backup_file_name##*.}"
fi

same_backup=($(find "$backup_dir_name" -iname "*${backup_file_md5_value}*" 2>/dev/null))
if [ ${#same_backup[@]} -ne 0 ]; then
  same_backup_md5_value=$(cat ${same_backup[0]} | md5sum)
  if [ "$backup_file_md5_value" == "${same_backup_md5_value:0:32}" ];then 
    echo '[true, "跳过, 已存在相同内容备份: '${same_backup[@]}'"]'
    if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 0; else exit 0; fi
  fi
fi

echo -e "备份文件: $backup_file"
if [ "$super_user" == "true" ]; then 
  sudo cp $backup_file $backup_path
else
  cp $backup_file $backup_path
fi

code=$?
if [[ ! $code -eq 0 ]]; then
  echo '[false, "'$backup_file' -> '$backup_path'文件备份异常, 返回状态: '$code'"]'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '[true, "完成备份: '$backup_path'"]'