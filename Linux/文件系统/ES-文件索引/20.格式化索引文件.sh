#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

md5sum=($(date +%Y%m%d%H%M%S%s | md5sum))

esdoc_path="$PWD/Linux/文件系统/ES-文件索引/-${md5sum[0]}.txt"
touch $esdoc_path

file_indexs_path=$(jq -r '.["10"][1]' $1)

items=($(jq -c -r '.["去重项"]|.[]' $file_indexs_path))

for item in ${items[@]}; do
  echo "$item" >> $esdoc_path
done

echo '[true, "'$esdoc_path'"]'