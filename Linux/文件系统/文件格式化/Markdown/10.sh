#!/bin/bash --login
eval "export $(jq -r '.["shell变量"]' $1)"

# 针对markdown文件进行一系列标准格式化
# 依赖gsed,gdate和fswatch

# 文件内容自动编辑涉及文件内容安全,容易造成严重损害
# 所以所有格式化命令必须经过大量测试才能应用于脚本批量执行
# 对于正则表达式,不仅仅要准确匹配,而且还要确保不会涵盖其他非目标内容
# 范围模糊不清但是能用的正则表达式,一律禁止使用,除非可以明确其匹配范围

if [[ ! $format_file_path =~ .*\.md$ ]]; then 
  echo '[false, "被格式化对象非Markdown文件"]'
  exit 255
fi

# 退出前操作内容
function before_exit () {
  # 清空屏幕
  printf "\\e[2J\\e[H\\e[m"
  echo '[true, ""]'
  exit 0
}

# 打印输出
function output_info () {
  echo $(gdate '+%Y-%m-%d %H:%M:%S.%N') $1;
}

# 标点格式化
function format_punctuation () {

  # 将中文句号替换为英文格式
  gsed -i -E "s/。$/./g" "$1"
  gsed -i -E "s/。/.<br>\n/g" "$1"

  # 将中文逗号替换为英文格式
  gsed -i -E "s/，/,/g" "$1"

  # 将中文感叹号替换为英文格式
  gsed -i -E "s/！/!/g" "$1"

  # 将中文括号替换为英文格式
  gsed -i -E "s/（/(/g" "$1"

  # 将中文括号替换为英文格式
  gsed -i -E "s/）/)/g" "$1"
  
  # 将中文冒号替换为英文格式
  gsed -i -E "s/：/:/g" "$1"
  
  # 将中文问号替换为英文格式
  gsed -i -E "s/？/?/g" "$1"

  # 将中文分号替换为英文格式
  # gsed -i -E "s/(^.*)；$/0. \1./g" "$1"
  gsed -i -E "s/；/;/g" "$1"

  # 标点前后空格去除
  # 0. 这种序号的空格也会被剔除
  # gsed -i -E "s/\s{0,}([,\"])\s{1,}/\1/g" "$1"

  # 行尾空格去除
  gsed -i -E "s/(^.*)(\s{1,})$/\1/g" "$1"

}

# 针对中文进行优化
function format_chinese () {
  # 将中文间空格剔除
  gsed -i -E "s/([^ -z])\s{1,5}([^ -z])/\1\2/g" "$1"

  # 中文与英文或数字间空格剔除
  gsed -i -E "s/([^ -z])\s{1,5}((\w|\d)+)\s{0,}/\1\2/g" "$1"

  # 中文与英文或数字间空格剔除
  gsed -i -E "s/\s{0,}((\w|\d)+)\s{1,5}([^ -z])/\1\3/g" "$1"

  gsed -i -E "s/告诉我们/表示/g" "$1"
  gsed -i -E "s/我们认为//g" "$1"

  # 将称谓词剔除 你我 这种替换会存在不通顺风险
  gsed -i -E "s/(让|使|)(您|你|我|他)(们|的|们的|)([^国])/\4/g" "$1"

}

# 针对数字进行优化
function format_number () {
  # 将数字间空格剔除
  gsed -i -E "s/([0-9])\s{1,5}([0-9])/\1\2/g" "$1"
}

# 内容格式化
function format_content () {

  # 段落前空格去除
  # Markdown的TOC会受到影响
  # gsed -i -E "s/^\s{1,}//g" "$1"

  # 将引号中内容转为斜体
  gsed -i -E "s/(\s|[A-Za-z]|[^ -z]|\uff1a|\u3002|\uff08|\uff09|^|\+|\-|,|:|\.)(‘'|“|”|″)/\1*/g" "$1"

  # 斜体星号前后添加空格
  gsed -i -E "s/([^ -z])\s{0,}\*(.+)\*\s{0,}([^ -z]|,|\.)/\1 *\2* \3/g" "$1"

  # 删除ibooks引述以及版权信息
  # sed -i -E "s/^\*//g" "$1"
  # sed -i -E "s/\*$//g" "$1"
  # gsed -i "/摘录来自:.*Apple Books.*$/d" "$1"

}

# 针对代码进行格式化
function format_code () {

  # JSON格式相关优化
  # 键和冒号中间不存在空格
  gsed -i -E "s/(\")\s{1,}(\:)/\1\2/g" "$1"
  # 冒号和值中间存在一个空格
  gsed -i -E "s/(\")\s{0,}(\:)\s{2,}(\"|[0-9]{1,})/\1\2 \3/g" "$1"
  # 大括号和双引号之间不存在空格
  gsed -i -E "s/(\{)\s{1,}(\")/\1\2/g" "$1"
  gsed -i -E "s/(\")\s{1,}(\})/\1\2/g" "$1"

}

# 格式化后调整
function format_after () {
  # 参考资料链接格式化
  gsed -i -E "s/(>\s\[)([^|]*)\|(.*])(\(.*\))$/\1\2 | \3\4/g" "$1"
}

function exec_format (){
  output_info "格式化内容开始执行.";
  format_punctuation "$@";

  format_content "$@";

  format_chinese "$@";

  format_number "$@";

  format_code "$@";

  format_after "$@";

  output_info "格式化内容执行完毕."  
}

function listien_change () {

  output_info "开始监听文件变化";
  
  # 命令执行一次即退出可有效防止因脚本修改文件内容导致重复执行
  fswatch -1 "$format_file_path" | while read file
  do
    output_info "捕获到 ${file##*/} 文件变化";
    echo "开始20秒等待"
    sleep 20
    start_time=$(gdate +%s.%N)
    exec_format "$file";
    end_time=$(gdate +%s.%N)
    output_info "脚本耗时:$(echo "$end_time - $start_time" | bc)";
  done
  output_info "本次监听结束";
  echo -e "\n";
}

# 捕获到 SIGINT 即正常退出
trap "before_exit;" SIGINT;
# 标记是否监听是否运行中
in_exec=0
while :
do
  if ([[ $in_exec -eq 0 ]])
  then
    in_exec=1
    listien_change;
    in_exec=0
  fi
done

echo '[true, ""]'