#!/bin/bash --login

sudo apt-get update
sudo apt-get install -fy google-chrome-stable
if [[ $? -eq 0 ]];then echo '[true, ""]';exit 0; fi

wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/chrome.list <<-"EOF"
deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main
deb [arch=amd64] https://repo.fdzh.org/chrome/deb/ stable main
EOF

sudo apt-get update
sudo apt-get install -fy google-chrome-stable
if [[ $? -eq 0 ]];then echo '[true, ""]';exit 0; fi

wget -t $(jq -r '.["全局参数"]["重试次数"]' $2) https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
if [[ ! $? -eq 0 ]];then echo '[false, "安装失败"]'; exit 255;fi

rm -f ./google-chrome-stable_current_amd64.deb

echo '[true, ""]'

# 容器内启动:
# google-chrome --no-sandbox
