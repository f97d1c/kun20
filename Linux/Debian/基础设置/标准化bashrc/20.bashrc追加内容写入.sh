#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

if [ ! "$write_app_add" == "true" ]; then 
  echo '[true, ""]'
  exit 0;
fi

find -name 'bashrc追加内容.sh' -type f -print0 | while IFS= read -r -d $'\0' file; do 
  dir_name=${file%/*}
  dir_1_name=${dir_name%/*}
  dir_2_name=${dir_name##*/}
  echo "$dir_2_name 相关内容追加"
  echo -e "\n\n# === $dir_2_name相关追加内容开始 ===" >> $target_path
  echo -e "\n$(cat $file)" >> $target_path
  echo -e "\n# === $dir_2_name相关追加内容结束 ===" >> $target_path
done

echo "完成替换, 需手动 source ~/.bashrc 或重开终端以生效."

echo '[true, ""]'