#!/bin/bash --login

case $hardware_platform in
  'x86_64')
    url_node='ubuntu'
  ;;
  'armv7l'|'aarch64')
    url_node='ubuntu-ports'
  ;;
  *)
    echo "尚未兼容当前硬件架构($hardware_platform)."
    return 255
  ;;
esac

index=$1
version_name=$(lsb_release -c -s)

if [ ! "$version_name" ];then
  echo "系统版本名称不存在."
  return 255
fi

function source_template () {
  echo "
    # 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
    deb $source_host/$url_node $version_name main restricted universe multiverse
    # deb-src $source_host/$url_node $version_name main restricted universe multiverse
    deb $source_host/$url_node $version_name-updates main restricted universe multiverse
    # deb-src $source_host/$url_node $version_name-updates main restricted universe multiverse
    deb $source_host/$url_node $version_name-backports main restricted universe multiverse
    # deb-src $source_host/$url_node $version_name-backports main restricted universe multiverse
    deb $source_host/$url_node $version_name-security main restricted universe multiverse
    # deb-src $source_host/$url_node $version_name-security main restricted universe multiverse

    # 预发布软件源，不建议启用
    # deb $source_host/$url_node $version_name-proposed main restricted universe multiverse
    # deb-src $source_host/$url_node $version_name-proposed main restricted universe multiverse
  "
}

# siteNames=('tsinghua:-:清华站点:-:稳定,推荐使用' 'net_ease:-:网易镜像:-:仅支持x86_64平台')
# for i in "${!siteNames[@]}"; do
#   arr=(${siteNames[$i]//:-:/ })
#   printf "%s\t%s\n" "$i" "${arr[1]} : ${arr[2]}"
# done

# if [ ! $index ];then
#   echo -n "请选择目标站点编号:";read -e index;
# fi

# arr=(${siteNames[$index]//:-:/ })
# siteName=${arr[0]}

case $1 in
  '清华')
    source_host='https://mirrors.tuna.tsinghua.edu.cn'
  ;;
  '网易')
    source_host='http://mirrors.163.com'
  ;;
  *)
    echo "无效参数: 输入站点名称对应内容为空."
    return 255
  ;;
esac

source_conent=$(source_template | awk '{gsub(/^\s+|\s+$|^\n$/, "");print}')

if [ ! "$write_cnfile" == "true" ]; then 
  echo "$source_conent"
fi

source_file_path='/etc/apt/sources.list'