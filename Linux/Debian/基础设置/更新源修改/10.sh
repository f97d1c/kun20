#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

can_used=('ubuntu' 'deepin')
system_name=$(cat /etc/issue | awk '{print tolower($1)}')
hardware_platform=$(uname -i)

case "${can_used[@]}" in  
  *"$system_name"*)
    child_script="Linux/Debian/基础设置/更新源修改/${system_name}_source.sh"
  ;;
  *)
    echo '[false, "尚未适配'$system_name'系统."]'
    exit 255
  ;;
esac
if [[ ! $? -eq 0 ]];then exit 255;fi

source $child_script "$site_name"
if [[ ! $? -eq 0 ]];then 
  echo '[false, "子脚本返回状态异常"]'; exit 255
fi

if [ ! "$write_cnfile" == "true" ]; then 
  echo '[true, ""]'; exit 0
fi

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$source_file_path'", "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

echo -e "开始写入源文件"
echo -e "$source_conent" | sudo tee $source_file_path

# TODO: 这里如果写入失败应该回滚文件但可能性不大
if [[ ! $? -eq 0 ]];then 
  echo '[false, "'$source_file_path' 文件写入异常"]'; exit 255
fi

echo '[true, "文件更新完成"]'
