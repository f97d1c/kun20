#!/bin/bash --login

echo '清理所有软件缓存'
sudo apt autoclean
sudo apt clean
echo '删除系统不再使用的孤立软件'
sudo apt autoremove -y
echo '删除所有apt/lists下的文件'
# 就此镜像而言,这条命令在添加后可以使镜像减小423MB
sudo rm -rf /var/lib/apt/lists/*
echo '清理孤立的库文件'
sudo apt -y remove --purge

echo '[true, ""]'