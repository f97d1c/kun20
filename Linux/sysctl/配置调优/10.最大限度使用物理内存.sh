#!/bin/bash --login

# https://www.cnblogs.com/xuanbjut/p/11479797.html

# linux 2.6+的核心会使用硬盘的一部分做为SWAP分区,
# 用来进行进程调度--进程是正在运行的程序--把当前不用的进程调成 等待(standby) ,甚至 睡眠(sleep),
# 一旦要用,再调成 活动(active),睡眠的进程就躺到SWAP分区睡大觉,把内存空出来让给 活动的进程.

# 如果内存够大,应当告诉 linux 不必太多的使用 SWAP 分区, 可以通过修改 swappiness 的数值.
# swappiness=0的时候表示最大限度使用物理内存,然后才是 swap空间,
# swappiness＝100的时候表示积极的使用swap分区,并且把内存上的数据及时的搬运到swap空间里面.

# 在ubuntu 里面,默认设置swappiness这个值等于60.

# /etc/sysctl.conf 添加:
# vm.swappiness=0