#!/bin/bash --login

if [[ "$(ps -p 1 -o comm=)" != 'systemd' ]];then echo '非宿主机跳过';exit 0;fi

apt install -y nginx

sudo cp 网络/网页服务/Nginx/nginx.conf /etc/nginx/nginx.conf

sudo nginx -t

if [[ ! $? -eq 0 ]];then 
  echo '[false, "Nginx配置检测异常"]'; exit 255
fi

sudo systemctl restart nginx