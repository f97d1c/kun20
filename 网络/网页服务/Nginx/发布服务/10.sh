#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

# domain_name="com.graphic.elasearch.database.local"
# project_root_path="/home/ff4c00/SpaceX/知识体系/应用科学/计算机科学/计算机系统/数据库/Elasticsearch/graphic-object-es/dist"
# index_page="index.html"
conf_name="${domain_name}_$port"

sudo tee /etc/nginx/sites-available/$conf_name <<EOF
server {
  listen $port;
  listen [::]:$port;

  root $project_root_path;
  index $index_page;

  server_name $domain_name;

  location / {
    try_files \$uri \$uri/ =404;
  }
}
EOF

sudo ln -s /etc/nginx/sites-available/$conf_name /etc/nginx/sites-enabled/ 2>/dev/null

sudo nginx -t

if [[ ! $? -eq 0 ]];then 
  echo '[false, "Nginx配置检测异常"]'; exit 255
fi

sudo systemctl restart nginx

# host_mapping=$(cat /etc/hosts | grep $domain_name)

# if [ ! "$host_mapping" ]; then
#   echo "127.0.0.1 $domain_name" | sudo tee -a /etc/hosts
# fi

curl -s "$domain_name:$port"

if [[ ! $? -eq 0 ]];then 
  echo '[false, "无法正常访问该服务"]'; exit 255
fi

echo ''
echo '[true, ""]'