#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

[ -e $conf_path ] || mkdir -p ${conf_path%/*};touch $conf_path

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$conf_path'", "必须备份": false, "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

tee "$conf_path" <<EOF
Host gitlab.com
  HostName gitlab.com
  User git
  # ProxyCommand bash -c "nc -v -x $(dig vpn.proxy.local +short):7891 %h %p"
  ServerAliveInterval 60
  ServerAliveCountMax 5

Host *
     ServerAliveInterval 86400
     ServerAliveCountMax 4
EOF
if [[ ! $? -eq 0 ]];then 
  echo '[false, "未正确写入文件"]'
  exit 255
fi

echo '[true, ""]'