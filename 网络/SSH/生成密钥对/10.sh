#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

if [[ -f "$store_path" ]] && [ "$overwrite" == 'false' ];then
  echo '[true, "该秘钥文件已存在: '$store_path'"]'
  exit 0
fi

ssh-keygen -t $public_key_algorithm -b $secret_key_bits -f $store_path
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '[true, ""]'