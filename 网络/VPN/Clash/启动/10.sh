#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

cd $clash_install_path
if [[ ! $? -eq 0 ]];then echo '[false, "文件夹切换失败"]'; exit 255;fi

pids=$(ps -ef | grep clash | grep -v grep | awk '{print $2}')
if [ $pids ]; then kill -9 $pids;fi

# ./clash*[^.gz]*v$clash_version -d config/ -f config/$config_file_name &>/dev/null &
./clash*[^.gz]*v$clash_version -d config/ -f $(ls config/*.yaml -lat | head -1 | tail -1 | awk '{print $9}') &>/dev/null &

echo '[true, ""]'