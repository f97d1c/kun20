#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

if [ ! -d "$clash_install_path" ];then mkdir $clash_install_path; fi

cnf_path="$PWD/$1"

cd $clash_install_path

if [ ! "$(find -name "*v$clash_version*")" ] ; then
  echo '下载Clash安装包'
  wget -t $(jq -r '.["全局参数"]["重试次数"]' $cnf_path) -c $clash_package_url
  if [[ ! $? -eq 0 ]];then echo '[false, "Clash安装包下载失败"]'; exit 255;fi
  echo 'Clash安装包解压'
  yes n | gunzip -k *v$clash_version.gz
  chmod +x clash*
fi

echo '下载Country.mmdb'
wget -nc -t $(jq -r '.["全局参数"]["重试次数"]' $cnf_path) -c -P "$clash_install_path/config" $clash_country_mmdb_url
if [[ ! $? -eq 0 ]];then echo '[false, "Country.mmdb下载失败"]'; exit 255;fi

if [ ! -d "$clash_install_path/dashboard" ];then
  echo '下载UI管理工具'
  mkdir "$clash_install_path/dashboard"
  wget -t $(jq -r '.["全局参数"]["重试次数"]' $cnf_path) -c -P "$clash_install_path/dashboard" $clash_ui_control_package_url
  if [[ ! $? -eq 0 ]];then echo '[false, "下载失败"]'; exit 255;fi

  echo 'UI管理工具安装包解压'
  cd $clash_install_path/dashboard/
  unzip -n *.zip
fi

echo '[true, ""]'