#!/bin/bash --login

local_address=$(jq -r '.["系统信息"]["局域地址"]' $2)
cmd="export https_proxy=http://$local_address:7890 http_proxy=http://$local_address:7890 all_proxy=socks5://$local_address:7891"

echo '[true, "'$cmd'"]'