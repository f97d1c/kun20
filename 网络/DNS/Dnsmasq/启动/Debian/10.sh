#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

conf_path='/etc/dnsmasq.conf'
dns_cnf_space_config=$(jq -r '.["基础配置"]' "$1")

public_dns_address=$(jq -r '.["公共DNS"]["地址"]' <<<$dns_cnf_space_config)
local_domain_cnf=$(jq -r '.["自建服务器"]["自定义域名"]' <<<$dns_cnf_space_config)

# 存在这个文件 下面配置的resolv-file不起作用
sudo rm -f /etc/resolv.conf

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$conf_path'", "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

echo -e "\n编辑配置文件: $conf_path"

dns_ips=($(jq -r 'keys_unsorted[]' <<<$public_dns_address))

listen_addresses=($listen_addresses)

sudo tee "$conf_path" <<EOF
resolv-file=$dns_config_path
strict-order
cache-size=10000
listen-address=127.0.0.1,$(printf "%s," "${listen_addresses[@]}" | sed 's/,$//g')

address=/$test_dns/${listen_addresses[0]}
EOF


defined_local_address=($(jq -c -r --arg hostname $(hostname) '[.["\($hostname)"]["指向本地"]] | flatten(1) | .[]' <<<$local_domain_cnf))

for listen_address in "${listen_addresses[@]}"; do
  for address in "${defined_local_address[@]}"; do
    echo "address=/$address/$listen_address" | sudo tee -a $conf_path
  done
  echo '' | sudo tee -a $conf_path
done

defined_hosts=($(jq -c -r --arg hostname $(hostname) '.["\($hostname)"] | [keys_unsorted[]] | map(select(. != "指向本地")) | .[]' <<<$local_domain_cnf))

for host in "${defined_hosts[@]}"; do
  defined_host_address=($(jq -c -r --arg hostname $(hostname) --arg host $host '[.["\($hostname)"]["\($host)"]] | flatten(1) | .[]' <<<$local_domain_cnf))
  for address in "${defined_host_address[@]}"; do
    echo "address=/$address/$host" | sudo tee -a $conf_path
  done
done

echo '' | sudo tee -a $conf_path

for ip in "${dns_ips[@]}"; do
  array=($(jq -c -r --arg key $ip '[.["\($key)"]] | flatten(1) | .[]' <<<$public_dns_address))
  if [[ ${#array[@]} -eq 0 ]];then continue; fi

  for domain in "${array[@]}"; do
    if [ $domain == '' ];then continue; fi
    if [ $domain == 'null' ];then continue; fi
    echo "server=/$domain/$ip" | sudo tee -a $conf_path
  done  
done

echo -e "\n编辑上游DNS服务器配置文件: $dns_config_path"

sudo tee "$dns_config_path" <<EOF
nameserver 127.0.0.1
nameserver 223.5.5.5
$(for ip in "${dns_ips[@]}"; do
  echo "nameserver $ip"
done)
EOF

echo -e "\n重启Dnsmasq"
sudo service dnsmasq restart

echo -e "\n查看Dnsmasq状态"
echo -e "\n$(sudo service dnsmasq status)"

if [[ ! "${listen_addresses[@]}" =~ "$(dig $test_dns +short)" ]]; then
  echo $(dig $test_dns +short)
  echo ${listen_addresses[@]}
  echo '[false, "配置异常, '$test_dns' 配置未生效"]'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '[true, ""]'
