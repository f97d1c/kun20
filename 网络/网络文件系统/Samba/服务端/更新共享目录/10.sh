#!/bin/bash --login
eval "export $(jq -r '.["shell变量"]' $1)"

samba_cnf_params=$(jq -r --arg config_key $config_key '.["基础配置"]["\($config_key)"]
' $1)

if [ "$samba_cnf_params" == 'null' ]; then
  echo '[false, '"尚不存在关于配置标签:$config_key,相关服务端配置信息."']'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

exports_samba_cnf_params=$(jq -r '.["共享目录"]' <<<$samba_cnf_params)

if [ ! "$exports_samba_cnf_params" ]; then
  echo '[false, '"尚不存在关于配置标签:$config_key,相关服务端共享目录信息."']'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$conf_path'", "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

printf '' | sudo tee $conf_path

export_dirs=($(jq -r 'keys_unsorted[]' <<<$exports_samba_cnf_params))

OLD_IFS="$IFS"
IFS=$'\n'

for dir in ${export_dirs[@]}; do
  export_contents=$(jq -r --arg dir $dir '.["\($dir)"]' <<<$exports_samba_cnf_params)
  export_content_keys=($(jq -r '. | keys_unsorted[]' <<<$export_contents))
  echo -e "\n\n[$dir]" | sudo tee -a $conf_path
  for key in ${export_content_keys[@]}; do
    value=$(jq -r --arg key $key '.["\($key)"]' <<<$export_contents)
    echo "  $key = $value" | sudo tee -a $conf_path
  done
done

IFS="$OLD_IFS"

sudo service smbd restart

echo '[true, ""]'