#!/bin/bash --login

if ! [ -f "/etc/init.d/nfs-kernel-server" ]; then
  echo '[false, "尚未安装nfs-kernel-server"]'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

eval "export $(jq -r '.["shell变量"]' $1)"
nfs_cnf_params=$(jq -r --arg host_tag $host_tag '.["基础配置"]["\($host_tag)"]' "$1")
conf_path='/etc/exports'

if [ "$nfs_cnf_params" == 'null' ]; then
  jq -r '.["基础配置"]' "$1"
  echo '[false, "尚不存在关于: '$host_tag',相关服务端配置信息."]'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

exports_nfs_cnf_params=$(jq -r '.["共享目录"]' <<<$nfs_cnf_params)

if [ "$nfs_cnf_params" == 'null' ]; then
  echo '[false, "尚不存在关于: '$host_tag',相关服务端共享目录信息."]'
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$conf_path'", "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

printf '' | sudo tee $conf_path

export_dirs=($(jq -r 'keys_unsorted[]' <<<$exports_nfs_cnf_params))

for dir in ${export_dirs[@]}; do
  export_hosts=($(jq -r --arg dir $dir '.["\($dir)"] | keys_unsorted[]' <<<$exports_nfs_cnf_params))

  for host in ${export_hosts[@]}; do
    is_domain=$(echo "$host" | grep -P "(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,}|xn--[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])$)")

    # dns自定义域名nfs无法解析 这里将其转为IP
    if [ "$is_domain" ];then
      host_ip=$(dig "$is_domain" +short)
      if [ ! "$host_ip" ];then
        echo "跳过, 无法解析 $host 对应IP地址"
        continue;
      fi
    fi
    
    export_options_=($(jq -r -c --arg dir $dir --arg host $host '.["\($dir)"]["\($host)"] | .[]' <<<$exports_nfs_cnf_params))

    export_options=$(printf "%s," "${export_options_[@]}")
    if [ "$is_domain" ];then
      echo "$dir $host_ip($export_options)" | sudo tee -a $conf_path
    else
      echo "$dir $host($export_options)" | sudo tee -a $conf_path
    fi

  done

done

sudo systemctl start rpc-statd

sudo exportfs -rv

echo '[true, ""]'