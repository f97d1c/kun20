#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

if [ ! "$(cat $cnf_path | grep "Match group $group_name")" ];then
sudo tee -a "$cnf_path" <<EOF
Match group $group_name
  ChrootDirectory /home
  X11Forwarding no
  AllowTcpForwarding yes
  ForceCommand internal-sftp
EOF

echo '重启SSH服务'
sudo systemctl restart ssh
fi

if [ ! "$(grep $group_name: /etc/group)" ]; then
  echo "添加$group_name用户组"
  sudo addgroup $group_name
fi

if [ ! "$(grep $user_name: /etc/passwd)" ]; then
  echo "$group_name用户组新增用户"
  sudo useradd -m $user_name -g $group_name

  read -s -p "新用户密码:" password
  echo "$user_name:$password" | sudo chpasswd

  echo '新用户文件夹权限调整'
  sudo chmod 700 /home/$user_name/
  # sudo chmod 777 -R /home/sftp_user/
fi

echo '[true, ""]'
