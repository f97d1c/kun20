#!/bin/bash --login

# 挂载数据卷中使用现对路径时该相对路径非脚本执行时路径
# 将$PWD替换为鲲的绝对路径

# sed 替换可以通过 / # _ @ * 进行分割
# $PWD中包含/需要替换分隔符
sed -i "s#\$PWD#$PWD#g" $(jq -r '.["10"][1]' $1)

cat $(jq -r '.["10"][1]' $1)

echo '[true, ""]'