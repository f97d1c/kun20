#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

repositories=($(jq -r '.["镜像版本"]|keys_unsorted[]' $1))

for repository in "${repositories[@]}";do 
  tags=($(jq -r --arg key $repository '.["镜像版本"]["\($key)"]|.[]' $1))
  for tag in "${tags[@]}";do 
    match_repository=$(docker images | grep $repository)
    if [ "$match_repository" ];then 
      match_tag=$(docker images | grep $tag)
      if [ "$match_tag" ];then 
        echo "[跳过] 已存在 $repository:$tag"
        continue
      fi 
    fi 

    curl -s -L $script_url | bash -s -- "$repository" "$repository:$tag"
    if [[ ! $? -eq 0 ]];then echo '[false, "脚本执行失败"]'; rm -rf $repository; exit 255;fi
    
    tar -cC "$repository" . | docker load
    if [[ ! $? -eq 0 ]];then echo '[false, "镜像加载失败"]'; exit 255;fi

    if [ "$auto_clean" == "true" ]; then rm -rf $repository; fi
  done
done

echo '[true, ""]'