#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

build_cnf_path="$PWD/虚拟化/Docker/镜像/Ubuntu/00.json"

if [ $build_mode == '开发' ]; then
  build_str="docker build \
  -t $depository_name:$image_tag \
  -f $docker_file_path \
  --build-arg $(echo $(jq -r '."SHELL变量"' $1) | awk '{gsub(/\s/, " --build-arg ");print}') \
  --build-arg BUILD_CNF_PATH=$build_cnf_path \
  "
else
  build_str="sudo docker buildx build \
  -t $depository_name:$image_tag \
  -f $docker_file_path \
  --build-arg $(echo $(jq -r '."SHELL变量"' $1) | awk '{gsub(/\s/, " --build-arg ");print}') \
  --build-arg BUILD_CNF_PATH=$build_cnf_path \
  "
fi

if [ $build_mode == '开发' ]; then
  build_str+="--squash "
elif [ $build_mode == '准生产' ]; then
  build_str+="--load "
elif [ $build_mode == '生产' ]; then
  echo "登录DockerHub账号"
  docker login
  if [[ ! $? -eq 0 ]]; then
    echo 'DockerHub登录失败, 请尝试手动执行: docker login'
    exit 255
  fi
  build_str+="--platform $(jq -r '."适配平台架构" | join(",")' $1) --push "
  
  # 未安装多平台模拟器时应先运行 app/docker/初始化Buildx.sh
  echo '启动多平台模拟器'
  docker run --privileged --rm tonistiigi/binfmt --install all
else
  echo -e "$show_how"
  described.error "构建模式错误, 当前值: $build_mode"
  exit 255
fi
build_str+="."

echo "$build_str"
eval "$build_str"

echo -n "是否运行新镜像(y/n)?"
read -e choice
case ${choice} in
y | Y | yes | yes | YES)
  docker run -t -i -v $PWD:/home/$user_name/kun $depository_name:$image_tag
  exit 0
  ;;
*)
  echo "已完成构建"
  exit 0
  ;;
esac

echo '[true, ""]'