let argv1 = JSON.parse(process.argv.slice(2)[0])
let Table = require('cli-table3');

let table = new Table({
  head: ['参数名', '参数值'],
  style: {
    head: [],
  }
});

for (let [key, value] of Object.entries(argv1)) {
  let str_value = JSON.stringify(value)
  if (str_value.length > 80){
    str_value = str_value.slice(0,80)+'...'
  }
  table.push([key, str_value])
} 

console.log(JSON.stringify([true, '\n运行时参数:\n'+table.toString()], null, 2))