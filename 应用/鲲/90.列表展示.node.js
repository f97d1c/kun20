let JsonRW = require("../../编程语言/NodeJs/文件系统/读写JSON对象.node")

let Table = require('cli-table3');
let argv1 = new JsonRW({文件绝对路径: process.argv.slice(2)[0]}).文件内容;
let argv2 = process.argv.slice(3)[0]
let argv3 = process.argv.slice(4)[0]

let jsonRW = new JsonRW({文件绝对路径: argv1.全局参数.项目根路径+'/应用/鲲/00.json'});
let table = null

function full_table(){
  table = new Table({
    head: jsonRW.文件内容.快速拨号.表格.表头,
    style: {
      head: [],
    }
  });
  for (let item of jsonRW.文件内容.快速拨号.表格.行元素) {
    table.push(item)
  }  
}

function match_table(indexs, match_keys){
  let match_object = {}
  for (const [key, value] of Object.entries(indexs)) {
    if (!match_keys.includes(key)) continue;
    match_object[key] = value
  }
  
  let keys_array = Object.keys(match_object).map(item => { return item.split('/') })
  let max_length = keys_array.reduce((x, y) => Math.max(x, y.length), 0)
  let head = [...Array(max_length).keys()].map(item => { return '路径'+(item+1) })

  table = new Table({
    head: [head, '快速拨号'].flat(),
    style: {
      head: [],
    }
  })

  for (let [key, value] of Object.entries(match_object)) {
    table.push([Object.assign(new Array(max_length).fill("-"), value.路径), value.快速拨号].flat())
  }

}

let indexs = jsonRW.文件内容.快速拨号.索引

if (!argv2){
  full_table()
}else if(argv3 && indexs[argv3]){
  console.log(JSON.stringify([true, indexs[argv3]], null, 2))
}else{
  let reg = new RegExp('.*'+argv2+'.*', 'ig')
  let match_keys = Object.keys(indexs).filter(str => { return str.match(reg) })

  if (match_keys.length == 0){
    full_table()
  }else if (match_keys.length == 1){
    console.log(JSON.stringify([true, indexs[match_keys[0]]], null, 2))
  }else if (match_keys.length > 1){
    match_table(indexs, match_keys)
  }

}

if (table) console.log(JSON.stringify([false, table.toString()], null, 2))