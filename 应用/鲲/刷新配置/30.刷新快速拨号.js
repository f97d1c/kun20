let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;
const path = require('path');
let jsonRW = new JsonRW({文件绝对路径: path.join(__dirname, '../00.json')});

let self_desc = jsonRW.文件内容.自描述
let k_call = {}
k_call['表格'] = {}
k_call['索引'] = {}

let keys_array = Object.keys(self_desc).map(item => { return item.split('/') })
let max_length = keys_array.reduce((x, y) => Math.max(x, y.length), 0)

head = [...Array(max_length).keys()].map(item => { return '路径'+(item+1) })

k_call['表格']['表头'] = [head, '快速拨号'].flat()
k_call['表格']['行元素'] = []

let index = -1
for (let [key, value] of Object.entries(self_desc)) {
  let key_ = 'k'+('00' + (index += 1)).slice(-3)
  options = Object.assign(new Array(max_length).fill("-"), key.split('/'))
  k_call['表格']['行元素'].push([options, key_].flat())
  k_call['索引'][key] = value
  k_call['索引'][key_] = value

  k_call['索引'][key].快速拨号 = key_
  k_call['索引'][key_].快速拨号 = key_
  
}

jsonRW.文件内容.快速拨号 = k_call
jsonRW.update()

console.log(JSON.stringify([true, jsonRW]))
