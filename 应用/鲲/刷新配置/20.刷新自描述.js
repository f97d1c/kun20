const path = require('path');

let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;
const execSync = require('child_process').execSync;
let dirs = JSON.parse(execSync("tree -J", { encoding: 'utf8' }))[0]
let jsonRW = new JsonRW({ 文件绝对路径: path.join(__dirname, '../00.json') });

result = {}

let set_result = (content, items = []) => {
  let str = items.flat().join('/')
  let c_files = content.contents.map(item => { return item.name })
  result[str] = {
    相对路径: str,
    路径: items,
    子文件: c_files,
    自描述: null,
    运行环境: ['宿主', '虚拟'],
    超管运行: false,
  }

  if (c_files.includes('01.自描述.json')) {
    let jsonRW = new JsonRW({ 文件绝对路径: path.join(__dirname, '../../../' + str + '/01.自描述.json') });
    let desc = jsonRW.文件内容

    if (desc.参数继承) {
      [desc.参数继承].flat().forEach(path_ => {
        let f_path = path.join(__dirname, '../../../' + path_ + '/01.自描述.json')
        f_desc = new JsonRW({ 文件绝对路径: f_path }).文件内容
        desc.参数 = Object.assign((f_desc.参数 || {}), desc.参数)
      })
    }

    if (desc.运行环境) result[str].运行环境 = desc.运行环境
    if (desc.超管运行) result[str].超管运行 = desc.超管运行

    result[str].自描述 = desc
  }

}

let iteration = (content, items = []) => {
  if (content.type != 'directory') return;
  if (content.name.match(/^\-.*/)) return;
  if (!content.contents.map(item => { return item.type }).includes('directory')) {
    return set_result(content, [items, content.name].flat())
  } else {
    for (content_ of content.contents) {
      if (content_.type != 'directory') continue;
      if (jsonRW.文件内容.应用.鲲.扫描忽略文件夹.includes(content_.name)) continue;
      iteration(content_, [items, content.name].flat())
    }
  }
}

for (content of dirs.contents) {
  if (content.type != 'directory') continue;
  if (content.name.match(/^\-.*/)) continue;
  if (jsonRW.文件内容.应用.鲲.扫描忽略文件夹.includes(content.name)) continue;
  iteration(content)
}

jsonRW.文件内容.自描述 = result
jsonRW.update()

console.log(JSON.stringify([true, result]))
