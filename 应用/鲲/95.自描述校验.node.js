
const { Described } = require("../../编程语言/NodeJs/自描述/引入.node");
const fs = require("fs");

let argv1 = JSON.parse(process.argv.slice(2)[0])

let argv2 = process.argv.slice(3)[0]
let content
try {
  if (fs.existsSync(argv2)) {
    content = fs.readFileSync(argv2, 'utf8')
  }else{
    content = argv2 || '{}'
  }
  argv2 = JSON.parse(content)
} catch (error) {
  argv2 = {}
}

let desc = new Described(argv1)

let res = desc.validate(argv2)
if (!res[0]){
  console.log(JSON.stringify(res, null, 2))
  process.exit(255)
}

let result = {}

for (key of Object.keys(desc.参数)){
  result[key] = desc[key]
  var_name = desc['参数'][key].变量名
  if (var_name) result[var_name] = desc[var_name]
}

result['shell变量'] = desc['shell变量']
result['SHELL变量'] = desc['SHELL变量']

console.log(JSON.stringify([true, result], null, 2))
