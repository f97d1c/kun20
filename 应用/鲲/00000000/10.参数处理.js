let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")

let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;

let result = []

let iteration = (obj, items = []) => {
  if (typeof(obj) != 'object') return result.push([items.join('/'), obj])

  for (let [key, value] of Object.entries(obj)) {
    iteration(value, [items, key].flat())
  }

}

for (let [key, value] of Object.entries(argv1.对象描述)) {
  iteration(value, [key])
}

console.log(JSON.stringify([true, result]))