const execSync = require('child_process').execSync;

let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;
let kun_info = new JsonRW({ 文件绝对路径: argv1["全局参数"]["项目根路径"]+'/应用/鲲/00.json' }).文件内容;

if (!argv1["10"][0]){
  console.log(JSON.stringify(argv1["10"]))
  process.exit(255)
}

for (let cmds of argv1["10"][1]) {
  try {
    // TODO: 这里有的命令需要超管权限执行 目前未发现隐患 或者在外面 sudo kun ....
    let function_info = kun_info['自描述'][cmds[0]]
    if(!!!function_info){
      console.log(JSON.stringify([false, cmds[0]+': 未找到对应信息']))
      process.exit(255)
    }

    let cmd = ['bash', 'kun', cmds].flat().join(' ')

    if(function_info["超管运行"]){
      cmd = ['sudo', cmd].join(' ')
    }

    let res = execSync(cmd, { encoding: 'utf8' }).replace(/\n$/, "")
    
    if (!res[0]){
      console.log(JSON.stringify(res))
      process.exit(255)
    }
  } catch (error) {
    console.log(JSON.stringify([false, "运行时异常", error]))
    process.exit(255)
  }
}

console.log(JSON.stringify([true, ""]))
