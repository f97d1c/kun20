const fs = require("fs");

const execSync = require('child_process').execSync;

let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = JSON.parse(process.argv.slice(2)[0])
let argv2 = process.argv.slice(3)[0]

if (!fs.existsSync(argv1.绝对路径)) {
  console.log(JSON.stringify([false, '绝对路径不存在', argv1]))
  process.exit(255)
}

let parts_ = execSync("find '" + argv1.绝对路径 + "' -type f -printf \"%f \n\" | sort -n | tr '\n' '/'", { encoding: 'utf8' }).replace(/\n$/, "")

let parts = parts_.split(" /")
parts.pop()

// tty: 是否输出过程日志
function cmd(cmd = "", tty = true) {
  try {
    if (tty) {
      cmd += " | tee /dev/tty"
    } else {
      cmd += " 2>/dev/null"
    }
    let res = execSync(cmd, { encoding: 'utf8' }).replace(/\n$/, "");
    if (typeof (res) == 'string') {
      return res.split("\n").slice(-1)[0]
    } else {
      return res
    }
  } catch (error) {
    let res = [
      false,
      error.message.split("\n"),
      error.status,
      // error.stderr, 
      // error.stdout
    ]
    console.log(JSON.stringify(res, null, 2))
    return res
  }
};

let resultFile = new JsonRW({ 文件绝对路径: argv1.绝对路径 + '/' + '00.json' });

let result

for (let file_name of parts) {
  let file_names = file_name.split('.')
  let suffix = file_names.slice(-1)[0]
  let file_index
  let describe

  if (file_name.match(/^\d{2,}\.[^\.]*\.[^\.]*$/)) {
    file_index = parseInt(file_names[0])
    describe = file_names[1]
  } else if ((file_name.match(/^\d{2,}\.[^\.]*$/))) {
    file_index = parseInt(file_names[0])
  } else if ((file_name.match(/^02\.[^.]*\.[^.]*\.sh$/))) {
    continue;
  } else if ((file_name.match(/^\-\d{2,}.*$/))) {
    continue;
  } else if ((file_name.match(/^[^.]*\..*$/))) {
    continue;
  } else {
    console.log(JSON.stringify([false, '文件名解析异常', file_name]))
    process.exit(255)
    // continue;
  }

  if (['txt', 'json'].includes(suffix)) continue;
  if (file_index < 10) continue;

  if (describe) console.log("开始执行: " + describe);
  let file_path = argv1.绝对路径 + '/' + file_name
  let params_path = argv1.绝对路径 + '/' + '00.json'

  if (suffix == 'sh') {
    result = cmd(['bash', "'" + file_path + "'", "'" + params_path + "'"].join(' '))
  } else if (suffix == 'js') {
    result = cmd(['node', "'" + file_path + "'", "'" + params_path + "'"].join(' '))
  } else if (suffix == 'rb') {
    result = cmd(['ruby', "'" + file_path + "'", "'" + params_path + "'"].join(' '))
  } else {
    console.log(JSON.stringify([false, '脚本后缀匹配失败', file_path]))
    process.exit(255)
  }

  if (!result) {
    console.log(JSON.stringify([false, '返回值为空', file_path]))
    process.exit(255)
  }

  try {
    if (typeof (result) == 'string') result = JSON.parse(result)
  } catch (error) {
    console.log(JSON.stringify([false, '返回值格式非标准格式', { "脚本相对路径": file_path, "返回内容": result }]))
    process.exit(255)
  }

  if (!Array.isArray(result)) {
    console.log(JSON.stringify([false, '返回值格式非标准格式', { "脚本相对路径": file_path, "返回内容": result }]))
    process.exit(255)
  }

  if (!result[0]) {
    console.log(JSON.stringify(result))
    process.exit(255)
  }

  resultFile.add(file_index, result)
}

if (!result) {
  console.log(JSON.stringify([true, '']))
} else if (!Array.isArray(result)) {
  console.log(JSON.stringify([true, '']))
} else if (result[0] == true && result[1] == '') {
  console.log(JSON.stringify([true, '']))
} else {
  console.log(JSON.stringify([true, '']))
}
