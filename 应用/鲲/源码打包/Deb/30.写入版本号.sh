#!/bin/bash --login

kun_path=$(cd `dirname $0`;pwd)

if [ "$kun_path" == '/usr/bin' ]; then
  echo "该脚本仅适用于源码打包"
  exit 255
fi

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../../../"
fi

eval "export $(jq -r '.["shell变量"]' $1)"

mkdir "$build_path/应用/鲲/版本号"
touch "$build_path/应用/鲲/版本号/10.sh"
tee "$build_path/应用/鲲/版本号/10.sh" <<EOF
#!/bin/bash --login
echo '[true, "$version_number"]'
EOF

echo '[true, ""]'