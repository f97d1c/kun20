#!/bin/bash --login

kun_path=$(cd `dirname $0`;pwd)

if [ "$kun_path" == '/usr/bin' ]; then
  echo "该脚本仅适用于源码打包"
  exit 255
fi

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../../../"
fi

eval "export $(jq -r '.["shell变量"]' $1)"

echo "创建版本文件夹: $build_path"
rm -rf $build_path
mkdir $build_path

echo "复制脚本至版本文件夹"
structure_keys=($(jq -r '.["10"][1]["项目根结构"] | keys_unsorted[]' $1))

for key in "${structure_keys[@]}";do
  file_type=$(jq --arg key $key -r '.["10"][1]["项目根结构"]["\($key)"]["type"]' $1)

  if [[ "$file_type" == "directory" ]];then
    mkdir $build_path/$key && cp -r $key/* $build_path/$key
  elif [[ "$file_type" == "file" ]];then
    cp $key $build_path
  fi
  
done

echo '[true, ""]'