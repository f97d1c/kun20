#!/bin/bash --login

kun_path=$(cd `dirname $0`;pwd)

if [ "$kun_path" == '/usr/bin' ]; then
  echo "该脚本仅适用于源码打包"
  exit 255
fi

if [ ! $kun_main_path ]; then
  current_path=$(cd `dirname $0`;pwd)
  kun_main_path="$current_path/../../../../"
fi

eval "export $(jq -r '.["shell变量"]' $1)"

cd $build_path

export USER=root
# export USER=ff4c00

dh_make --indep --createorig -y
grep -v makefile debian/rules > debian/rules.new
mv debian/rules.new debian/rules

echo kun usr/bin > debian/install
structure_keys=($(jq -r '.["10"][1]["项目根结构"] | keys_unsorted[]' $1))
for key in "${structure_keys[@]}";do
  file_type=$(jq --arg key $key -r '.["10"][1]["项目根结构"]["\($key)"]["type"]' $1)

  if [[ "$file_type" == "directory" ]];then
    # echo $key/ usr/lib/kun >> debian/install    
    echo $key/ $HOME/.kun >> debian/install    
  elif [[ "$file_type" == "file" ]];then
    # echo $key usr/lib/kun >> debian/install
    echo $key $HOME/.kun >> debian/install
  fi
done

echo "1.0" > debian/source/format
rm debian/*.ex
debuild -us -uc

mv "$build_path/../${packaged_name}-1_all.deb" "$build_path/../$packaged_name.deb"

echo '[true, ""]'