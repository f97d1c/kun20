#!/bin/bash --login

system_name=$(cat /etc/issue | awk '{print tolower($1)}')
tags=()

if [[ "$system_name" == "ubuntu" ]];then
  sudo apt update
  tags=('debian' 'ubuntu')
elif [[ "$system_name" == "deepin" ]];then
  sudo apt update
  tags=('debian' 'ubuntu' 'deepin')
else
  echo '[false, "尚未适配该系统名称: '$system_name'"]'
  exit 255
fi

for tag in "${tags[@]}";do 
  find -name "02.运行级依赖.$tag.sh" -type f -print0 | while IFS= read -r -d $'\0' file; do 
    echo "开始执行: $file"
    sudo bash $file
    code=$?
    if [ ! $code -eq 0 ];then 
      exit $code; 
      echo '[false, "运行时异常"]'
      exit 255
    fi
  done
done

echo '[true, ""]'