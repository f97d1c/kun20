#!/bin/bash --login

system_name=$(cat /etc/issue | awk '{print tolower($1)}')
tags=()

if [[ "$system_name" == "ubuntu" ]];then
  sudo apt update
  tags=('debian' 'ubuntu')
elif [[ "$system_name" == "deepin" ]];then
  sudo apt update
  tags=('debian' 'ubuntu' 'deepin')
else
  echo '[false, "尚未适配该系统名称: '$system_name'"]'
  exit 255
fi

for tag in "${tags[@]}";do 
  find -name "02.功能级依赖.$tag.sh" -type f -print0 | while IFS= read -r -d $'\0' file; do 
  # find ./应用/虚拟化/Docker/02.功能级依赖.debian.sh -type f -print0 | while IFS= read -r -d $'\0' file; do
    echo -e "\033[0;31m====== 开始执行: $file ======\033[0m"
    sudo bash $file
    code=$?
    if [ ! $code -eq 0 ];then 
      echo '[false, "运行时异常"]'
      exit $code;
    fi
  done
done

echo '[true, ""]'