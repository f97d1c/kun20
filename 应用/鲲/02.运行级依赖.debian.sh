#!/bin/bash --login

apt install -fy tree 
apt install -fy apt-utils
apt install -fy lsb-release
apt install -fy ca-certificates
apt install -fy gawk # 自带awk功能上有所缺失
# apt install -fy bsdmainutils # 将标准输入或文件格式化为多列(column命令)
apt install -fy jq
# apt install -fy gawk

echo '系统字符集配置'
apt install -fy locales
locale-gen zh_CN.UTF-8
