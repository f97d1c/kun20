let argv1 = JSON.parse(process.argv.slice(2)[0])
let Table = require('cli-table3');

if (Object.keys(argv1).length == 0){
  console.log(JSON.stringify([false, 'argv1对象为空']))
  process.exit(255)
}

let options = argv1.参数 || {}

let option_keys = [...new Set(Object.values(options).map(item => { return Object.keys(item) }).flat())]

if (option_keys.length == 0) option_keys = ['参数值'];

let table = new Table({
  head: ['参数名', option_keys].flat(),
  style: {
    head: [],
  }
});

for (let [key, value] of Object.entries(options)) {
  // table.push([key, option_keys.map(item => { return value[item] })].flat())
  table.push([key, option_keys.map(item => {
    if (item == '默认值') {
      let str_value = JSON.stringify(value[item]) || ''
      if (str_value.length > 40) {
        str_value = str_value.slice(0, 40) + '...'
      }
      return str_value
    } else if (item == '说明') {
      let str_value = value[item] || ''

      if (typeof str_value == 'object' && Array.isArray(str_value)){
        str_value = str_value.join("\n")
      }else if (typeof str_value == 'object'){
        str_value = JSON.stringify(str_value)
      }

      if (!!!str_value.match(/\n/) && str_value.length > 40) {
        str_value = str_value.slice(0, 40) + '...'
      }
      return str_value
    } else {
      str_value = value[item]
      return str_value
    }

  })].flat())
}

let head = '\n自描述信息'

Object.keys(argv1).filter(key => { if (!['参数'].includes(key)) { return key } }).forEach(key => {
  if (typeof argv1[key] == 'object' && !Array.isArray(argv1[key])) {
    head += '\n' + key + ': '
    for (let [key_, value_] of Object.entries(argv1[key])) {
      head += '\n  ' + key_ + ': ' + value_
    }
  } else {
    head += '\n' + key + ': ' + argv1[key]
  }
})

head += '\n参数信息:\n'

console.log(JSON.stringify([true, head + table.toString()], null, 2))