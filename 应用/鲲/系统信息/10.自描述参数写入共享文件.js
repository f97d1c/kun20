let JsonRW = require("../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;
const path = require('path');

const execSync = require('child_process').execSync;

for (let [key, value] of Object.entries(argv1)) {
  if (key.match(/^[a-z A-Z].*/)) {
    delete argv1[key]
    continue
  }

  if (['系统信息', '全局参数'].includes(key)) {
    continue
  }
  let value_ = execSync("eval 'echo " + value + "'", { encoding: 'utf8' }).replace(/\n/g, "")
  if (['硬件架构', '内核信息', '版本信息', '默认SHELL类型'].includes(key)) {
    argv1[key] = value_.toLowerCase().split(' ')
  } else {
    argv1[key] = value_
  }

}

let result = {
  "局域地址": execSync("hostname -I | awk '{print $1}'", { encoding: 'utf8' }).replace(/\n/g, ""),
  "硬件架构": argv1.硬件架构[0],
  "内核类型": argv1.内核信息[0],
  "内核版本": argv1.内核信息[2],
  "系统类型": [],
  "系统版本": argv1.版本信息[1],
  "虚拟环境": (argv1.虚拟环境 == '0')
}

if (argv1.版本信息[0] == 'ubuntu') {
  result.系统类型 = ['linux', 'debian', 'ubuntu']
}

let jsonRW = new JsonRW({ 文件绝对路径: path.join(__dirname, '../00.json') });

jsonRW.文件内容.系统信息 = result
jsonRW.update()

console.log(JSON.stringify([true, result]))