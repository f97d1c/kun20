#!/bin/bash --login

echo "$(jq -r '.["配置文件"]' $1)"

keys=($(jq -c -r '.["配置文件"] | keys_unsorted[]' $1))

conf_path='/etc/apcupsd/apcupsd.conf'

bash kun 'Linux/文件系统/文件备份' '{"目标文件": "'$conf_path'", "超管权限": true}'
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '' | sudo tee $conf_path

for key in "${keys[@]}";do 
  value=$(jq -r --arg key $key '.["配置文件"]["\($key)"]["参数值"]' $1)

  echo "$key $value"  | sudo tee -a $conf_path
done

sudo systemctl restart apcupsd
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '[true, ""]'