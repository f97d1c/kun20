#!/bin/bash --login

image_ids=($(docker images | grep none | awk '{if ($1=="<none>") print $3}'))

for index in "${!image_ids[@]}";
do
  i_id=${image_ids[$index]}
  printf "%s\t%s\n" "$index" "删除镜像: $i_id"
  error=$(docker rmi $i_id 2>&1 >/dev/null)
  if [[ $? -eq 0 ]];then continue; fi

  container_ids=($(echo $error | awk '{gsub(/Error .* by stopped container\s/, "");print}'))
  if [ ! "$container_ids" ];then continue; fi

  for i in "${!container_ids[@]}";
  do
    c_id=${container_ids[$i]}
    printf "%s\t%s\n" "$index-$i" "删除已停止容器: $c_id"
    docker rm $c_id 1>/dev/null 2>/dev/null
  done

  docker rmi $i_id 1>/dev/null 2>/dev/null
done

results=$(docker images | grep none | awk '{if ($1=="<none>") print}')

if [ ! "$results" ];then
  echo "所有空镜像已清除"
else 
  echo -e "$results"
fi

echo '[true, ""]'