#!/bin/bash --login

echo '安装Buildkit'
docker pull moby/buildkit
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '安装并启动多平台模拟器(全部)'
docker run --privileged --rm tonistiigi/binfmt --install all
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '创建基于docker-container驱动的builder实例kun_builder'

docker buildx create --name kun_builder --driver docker-container
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '切换至kun_builder'
docker buildx use kun_builder
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '[true, ""]'
