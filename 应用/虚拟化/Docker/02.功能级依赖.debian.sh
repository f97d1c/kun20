#!/bin/bash --login

if [[ "$(ps -p 1 -o comm=)" != 'systemd' ]];then echo '非宿主机跳过';exit 0;fi

echo '卸载旧版本Docker'
apt remove docker docker-engine docker.io containerd runc

echo '添加Docker的官方GPG密钥'
gpg_path='/usr/share/keyrings/docker-archive-keyring.gpg'
rm -f $gpg_path
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o $gpg_path

echo "deb [arch=$(dpkg --print-architecture) signed-by=$gpg_path] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update

apt-get install -fy docker-ce docker-ce-cli containerd.io

echo '以非Root用户身份管理Docker'
sudo groupadd docker
sudo usermod -aG docker $USER

echo '标准化配置文件(daemon.json)'
sudo cp ./应用/虚拟化/Docker/daemon.json /etc/docker/daemon.json

echo '检查当前配置'
sudo docker info

echo '重启服务'
sudo systemctl daemon-reload
sudo systemctl restart docker

# https://docs.docker.com/engine/install/ubuntu/