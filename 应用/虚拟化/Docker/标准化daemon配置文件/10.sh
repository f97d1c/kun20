#!/bin/bash --login

sudo touch /etc/docker/daemon.json
if [[ ! $? -eq 0 ]];then exit 255;fi

echo "添加国内镜像源并开启试验模式"

tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": [
    "https://qvmie8e5.mirror.aliyuncs.com",
    "https://hub-mirror.c.163.com",
    "https://mirror.baidubce.com"
  ],
  "experimental": true
}
EOF
if [[ ! $? -eq 0 ]];then exit 255;fi

export DOCKER_CLI_EXPERIMENTAL=enabled

sudo docker info
if [[ ! $? -eq 0 ]];then exit 255;fi

echo "重启服务"
sudo systemctl daemon-reload
if [[ ! $? -eq 0 ]];then exit 255;fi

sudo systemctl restart docker
if [[ ! $? -eq 0 ]];then exit 255;fi

echo '标准化daemon配置文件完成'

echo '[true, ""]'