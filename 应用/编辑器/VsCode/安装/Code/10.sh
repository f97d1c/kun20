#!/bin/bash --login

echo "更新包索引并安装依赖项"

sudo apt update
sudo apt install gnupg2 software-properties-common apt-transport-https wget

echo "导入 Microsoft GPG 密钥"

wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -

echo "启用 Visual Studio Code 存储库"

sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

echo "安装最新版本的 Visual Studio Code"

sudo apt update
sudo apt install code

echo '[true, ""]'