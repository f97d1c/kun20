git config --global user.name "ff4c00"
git config --global user.email ff4c00@gmail.com

# 账号缓存
git config --global credential.helper 'cache --timeout 36000000000'

# 禁用中文字符转义再显示
git config --global core.quotepath false

# 中文乱码问题处理
git config --global i18n.commitencoding utf-8
git config --global i18n.logoutputencoding utf-8
export LESSCHARSET=utf-8

# git/https 缓冲区设置(将缓冲区增加到 500MB)
git config --global http.postBuffer 524288000

# GIT提醒消息忽略(作用未知)
git config --global pull.ff false

# 将任何 git://github.com/(未加密的 Git 协议)更改为git@github.com:(SSH URL)
# https://stackoverflow.com/questions/70663523/the-unauthenticated-git-protocol-on-port-9418-is-no-longer-supported
git config --global url."https://github.com/".insteadOf git://github.com/
