
# 将rm命令替换为trash命令(trash-cli)
alias rm='trash-put' # 把文件或目录移到回收站
alias rl='trash-list' # 列出回收站文件
alias rr='trash-restore' # 恢复回收站文件
# trash-empty 清空回收站
# trash-rm 删除回收站文件