#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

echo "复制tmux配置文件"
sudo cp '应用/其他/Tmux/初始化/tmux.conf' "/home/$user_name/.tmux.conf"
sed -i "s/\${前缀}/$prefix/g" "/home/$user_name/.tmux.conf"

if [[ ! $? -eq 0 ]]; then
  echo "配置文件复制失败"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi

echo '刷新tmux配置文件'
tmux source-file "/home/$user_name/.tmux.conf" 2>/dev/null

echo '[true, "tmux完成初始化"]'
