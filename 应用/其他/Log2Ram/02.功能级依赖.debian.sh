#!/bin/bash --login
# https://github.com/azlux/log2ram
# https://mp.weixin.qq.com/s/aLaO-_iUpbGjihbr_uXP1A
# 安装 Log2Ram 后,日志不会直接写入磁盘,而是顾名思义,它们会写入 RAM.

# curl -L https://github.com/azlux/log2ram/archive/master.tar.gz | tar zxf -

# cd log2ram-master
# chmod +x install.sh
# sudo ./install.sh
# cd ..
# rm -rf log2ram-master

echo "deb [signed-by=/usr/share/keyrings/azlux-archive-keyring.gpg] http://packages.azlux.fr/debian/ bullseye main" | tee /etc/apt/sources.list.d/azlux.list
wget -O /usr/share/keyrings/azlux-archive-keyring.gpg  https://azlux.fr/repo.gpg
apt update
apt install log2ram

# 检查 log2ram 是否正在运行
# sudo systemctl status log2ram