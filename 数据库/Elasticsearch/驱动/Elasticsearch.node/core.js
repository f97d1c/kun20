const _request = require("./request")

class Core {
  constructor(params = {}) {
    let defaultValue = {
      请求协议: "http",
      主机IP: undefined,
      主机端口: 9200,
      索引名称: undefined,
      索引别名: undefined,
      索引类型: "doc",
      索引映射: undefined,
      请求地址: undefined,
      请求体: undefined,
      请求方式: "GET",
      请求头: {
        "content-type": "application/json"
      },
      重定向时: "follow",
      请求模式: "cors",
      文档路径: undefined,
      别名模式: true,
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

  reload(params){
    params = Object.assign(this, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

  request(params=this){
    this.reload(params)
    let result = _request(params)

    if (!result[0]){
      let e = new Error();
      result.push(e.stack.split("\n").slice(1).map(function(value) {return value.replace(/^\s+/, '') }))
    }

    return result 
  }
  
}

module.exports = Core;