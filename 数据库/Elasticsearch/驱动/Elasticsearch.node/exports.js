let Index = require("./index")
let Document = require("./document")

class Elasticsearch {
  constructor(params = {}) {
    let defaultValue = {
      request: require("./request"),
      index: new Index(params),
      document: new Document(params),
    }

    params = Object.assign(defaultValue, params)

    for (let [key, value] of Object.entries(params)) {
      this[key] = value
    }
  }

}

module.exports = Elasticsearch;