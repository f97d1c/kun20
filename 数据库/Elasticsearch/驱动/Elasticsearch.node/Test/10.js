const Elasticsearch = require("../exports")
let JsonRW = require("../../../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
let argv1 = new JsonRW({ 文件绝对路径: process.argv.slice(2)[0] }).文件内容;
const path = require("path");
let es = new Elasticsearch(argv1);

function handle_result(result = {}, expect_state = true) {
  if (result[0] == expect_state) return;

  let result_ = Object.assign([], result)
  if (!result[0] && result.length == 2){
    let e = new Error();
    result_.push(e.stack.split("\n").slice(1).map(function(value) {return value.replace(/^\s+/, '') }))
  }

  if (!result[0]) console.log(JSON.stringify(result_, null, 2));
  console.log(JSON.stringify(result));
  process.exit(255);
}

handle_result(es.request(argv1))
handle_result(es.index.create(), false)
handle_result(result = es.index.create(), expect_state = false)
handle_result(es.index.recreate({ 索引别名: 'test_index' }))
handle_result(es.index.mapping(), false)
handle_result(es.index.mapping({索引映射: {"properties": {"name": {"type": "keyword"}}}}))
handle_result(es.document.create(), false)
handle_result(es.document.create({ 索引别名: 'test_index'}), false)
handle_result(es.document.create({ 索引别名: 'test_index', 文档路径: './test.json'}), false)
handle_result(es.document.create({ 索引别名: 'test_index', 文档路径: path.resolve("./数据库/Elasticsearch/驱动/Elasticsearch.node/Test/_731.test_index.json")}))

// handle_result(es.index.remove({索引名称:'test_index'}))

console.log(JSON.stringify([true, '']));
