var request = require('sync-request');

function requestES(params={}) {
  params = Object.assign({
    "请求协议": "http",
    "主机IP": undefined,
    "主机端口": 9200,
    "索引类型": "doc",
    "请求地址": "",
    "请求方式": "GET",
    "请求头": {
      "content-type": "application/json"
    },
    "重定向时": "follow",
    "请求模式": "cors",
  }, params)

  if (params.请求地址 == 'undefined') {
    return [false, '请求地址不能为空', { params: params }]
  }

  if (typeof params.请求头 == 'string') params.请求头 = JSON.parse(params.请求头)

  let requestOptions = {
    method: params.请求方式,
    headers: params.请求头,
    redirect: params.重定向时,
    mode: params.请求模式,
  };

  if (params.请求体) requestOptions.body = JSON.stringify(params.请求体)

  // if (['POST', 'Post', 'post'].includes(params.请求方式)) {
  //   params.请求体 ||= {}
  //   if (typeof params.请求体 == 'string') params.请求体 = JSON.parse(params.请求体)
  //   requestOptions.body = JSON.stringify(params.请求体)
  // }

  let requestPath = params.请求协议 + '://' + params.主机IP + ':' + params.主机端口 + '/' + params.请求地址

  let res = request(params.请求方式, requestPath, requestOptions);
  let flag = !!res.statusCode.toString().match(/2\d{2}/) 

  let body
  if (flag) {
    body = JSON.parse(res.getBody('utf8'))
  } else {
    if (res.body) {
      body = JSON.parse(res.body)
    } else {
      body = res
    }
    console.log(JSON.stringify(res, null, 2))
  }

  return [flag,  body]
}

module.exports = requestES;