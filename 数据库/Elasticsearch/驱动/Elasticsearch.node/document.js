const Core = require("./core")
const JsonRW = require("../../../../编程语言/NodeJs/文件系统/读写JSON对象.node")
const fs = require('fs')
const execSync = require('child_process').execSync;

class Document extends Core {
  constructor(params = {}) {
    super(params)
  }

  create(params = {}) {
    this.请求方式 = 'POST'
    this.reload(params)

    if (!(this.索引名称 || this.索引别名)) return [false, '索引名称/别名不能为空']
    if (!this.文档路径) return [false, '文档路径不能为空']
    if (!fs.existsSync(this.文档路径)) return [false, this.文档路径 + ' 文档(绝对)路径不存在']

    let items = new JsonRW({ 文件绝对路径: this.文档路径 }).文件内容;
    let create_url = (this.索引别名 || this.索引名称) + '/_' + this.索引类型

    for (let index in items) {
      this.请求体 = items[index]
      let md5 = execSync('echo ' + JSON.stringify(this.请求体) + ' | md5sum', { encoding: 'utf8' }).slice(0, 32)
      this.请求地址 = create_url + '/' + md5
      let res = this.request()
      if (!res[0]) return res
    }

    return [true, '']
  }

  create_breakpoint(params = {}) {
    
  }

}

module.exports = Document;
