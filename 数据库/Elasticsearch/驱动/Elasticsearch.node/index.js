const Core = require("./core")

class Index extends Core {
  constructor(params = {}) {
    super(params)
  }

  create(params = {}) {
    this.请求方式 = 'PUT'
    this.reload(params)
    if (!(this.索引名称 || this.索引别名)) return [false, '索引名称/别名不能为空']

    if (!this.索引别名) {
      this.请求地址 = this.索引名称
      return this.request()
    }

    let date = new Date();

    this.索引名称 = this.索引别名 + '_' + date.toISOString().replace(/(.*)T.*/, '$1').replace(/\-/g, '')
    this.请求地址 = this.索引名称

    let res = this.request()
    if (!res[0]) return res

    // return [true, '']
    return this.alias()
  }

  recreate(params = {}) {
    let res = this.create(params)
    if (res[0]) return res

    if (res[1]["status"].toString().match(/4\d{2}/)) {
      return [false, this.请求地址, this.索引名称, this.索引别名]
      res = this.remove(params)
      if (!res[0]) return res
      return this.create(params)
    }

    return res
  }

  remove(params = {}) {
    this.请求方式 = 'DELETE'
    this.reload(params)
    return this.request()
  }

  alias(params = {}) {
    this.请求方式 = 'GET'
    this.请求地址 = '_alias/' + this.索引别名
    let res = this.request()
    if (res[0]) {
      this.请求地址 = this.索引别名 + '*/_alias/' + this.索引别名
      res = this.remove()
      if (!res[0]) return res
    }

    this.reload(params)
    this.请求方式 = 'PUT'
    this.请求地址 = this.索引名称 + '/_alias/' + this.索引别名
    return this.request()
  }

  mapping(params = {}) {
    this.请求方式 = 'PUT'
    this.reload(params)
    if (!this.索引映射) return [false, '索引映射不能为空']
    this.请求地址 = this.索引名称 + '/_mapping'
    this.请求体 = this.索引映射
    return this.request()
  }

}

module.exports = Index;