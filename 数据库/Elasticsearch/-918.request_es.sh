#!/bin/bash --login

function requestES(){
  if [[ ${2} == *"?"* ]]; then
    requestUl="$es_host:$es_port/${2}"
  else
    requestUl="$es_host:$es_port/${2}?pretty=true"
  fi
  start_at="开始时间: $(date '+%Y-%m-%d %H:%M:%S')\n"
  if [ ! -z "$3" ]; then
    # Json格式化处理需要安装: npm install -g json
    printf "\n\n$start_at请求方式: $1\n请求路径: $2\n完整路径: $requestUl\n参数:\n$(echo $3|json)"
    printf "返回结果:\n"
    res=$(curl -s -H "Content-Type: application/json" -X $1 $requestUl -d "$3" | tee /dev/tty)
    jq -c -r '.' <<<"$res"
  elif [ ! -z "$2" ]; then
    printf "\n\n$start_at请求方式: $1, 请求路径: $2\n完整路径: $requestUl"
    printf "返回结果:\n"
    res=$(curl -s -X $1 $requestUl | tee /dev/tty)
    jq -c -r '.' <<<"$res"

  else
    requestUl="$es_host:$es_port/${1}?pretty=true"
    printf "\n\n$start_at请求路径: $1\n完整路径: $requestUl"
    printf "返回结果:\n"
    res=$(curl -s "$requestUl" | tee /dev/tty)
    jq -c -r '.' <<<"$res"
  fi
}