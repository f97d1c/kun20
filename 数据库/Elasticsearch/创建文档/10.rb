#!/usr/bin/env ruby

requires = {
  # '终端工具' => 'irb',
  "断点调试" => "pry",
  "Http请求" => "rest-client",
  "JSON数据处理" => "json",
  "命令行选项分析" => "optparse",
  'JSON转Ruby对象' => 'ostruct',
# '格式化打印JSON数据' => 'awesome_print',
}

requires.each do |desc, gem_name|
  begin
    require gem_name
  rescue LoadError => e
    puts "缺少Gem: #{gem_name}(用于#{desc}), 正在尝试安装该Gem."
    `gem install #{gem_name}`
  end
end

file = File::open(ARGV[0])

arg1 = JSON.parse(file.read, object_class: OpenStruct)
file.close()


def judge_doc_present(es_search_url:, doc_id:)
  request_path = es_search_url
  body = { "query": { "ids": { "type": "_doc", "values": [doc_id] } } }
  headers = { 'Content-Type': "application/json" }
  # print "请求ES: #{body}\n"
  res = RestClient::Request.execute(method: "post", headers: headers, url: request_path, payload: body.to_json)
  # print "返回结果: #{res.body}\n"
  return false unless (res.code == 200)
  return false unless JSON.parse(res.body)["hits"]["hits"].length > 0
  true
end

# 普通字符串md5sum和hexdigest无区别 但是关于json格式数据,shell字符串不支持,所以bash和ruby的加密值也不同
def md5sum(str:)
  md5 = `str=#{str};md5=$(echo -n "$str" |md5sum);echo -n $md5`
  md5[0...32]
end

def get_breakpoint_index(array_file:, es_search_url:)
  judge_doc_present_ = lambda { |md5| judge_doc_present(es_search_url: es_search_url, doc_id: md5) }
  left_index = 0
  right_index = array_file.size - 1
  next_index = 0
  search_count = 0

  while (left_index <= right_index)
    search_count += 1
    mind = (left_index + right_index) / 2
    md5 = md5sum(str: (array_file[mind]||'').gsub("\n", ""))
    doc_flag = judge_doc_present_.call(md5)
    next_index = 0

    if (doc_flag && !judge_doc_present_.call(md5sum(str: (array_file[mind + 1]||'').gsub("\n", ""))))
      print "#{search_count}. 正中目标, 当前中位数: #{mind}\n"
      next_index = mind + 1
      right_index = -1
    elsif (doc_flag)
      print "#{search_count}. 向后搜索, 当前中位数: #{mind}\n"
      left_index = mind + 1
    elsif (!doc_flag)
      print "#{search_count}. 向前搜索, 当前中位数: #{mind}\n"
      right_index = mind - 1
    end
  end
  next_index
end

start_index = 0
array_file = File.readlines(arg1.ES文档路径)
es_host = "http://#{arg1.主机IP}:#{arg1.主机端口}"

es_search_url = "#{es_host}/#{arg1.索引名称}/_search"
es_create_url = "#{es_host}/#{arg1.索引名称}/_#{arg1.索引类型}"

if (arg1.断点续读)
  start_index = get_breakpoint_index(array_file: array_file, es_search_url: es_search_url)
end

if (array_file.length == start_index)
  p [true, "已完成全部文档创建"]
  exit 0
end

print "从#{start_index}开始创建索引.\n"
current_index = start_index-1
array_file[start_index..-1].each do |content|
  current_index+=1
  next unless content
  json_str = content.gsub("\n", "")
  md5 = md5sum(str: json_str)

  headers = { 'Content-Type': "application/json" }
  request_url = "#{es_create_url}/#{md5}"
  begin
    res = RestClient::Request.execute(method: "post", headers: headers, url: request_url, payload: json_str)
  rescue => e
    p [false, "接口请求异常", e.message]
    exit 255
  end

  flag = [200, 201].include?(res.code)
  body = (JSON.parse(res.body) rescue { error_msg: "返回内容解析失败", original: res })
  unless flag
    p [flag, "状态码异常", body] 
    exit 255
  end

  res = JSON.parse(res.body)
  print "#{current_index}: #{res['result']}, #{json_str}\n"
end

p [true, ""]