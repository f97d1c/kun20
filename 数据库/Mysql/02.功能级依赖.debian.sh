#!/bin/bash --login

# https://silenceszd.top/2020/05/19/Linux/Linux-deepin%E4%B8%AD%E5%AE%89%E8%A3%85mysql/
# 由于 deepin 系统仓库的原因,不能直接 apt 安装 mysql,清华源以及其他源都没有 mysql-server 和 mysql-client 两个包.不过我们可以用 mariadb 来代替 mysql.
# apt install -y mysql-client libmysqlclient-dev

# MariaDB数据库管理系统是 MySQL 的一个分支,主要由开源社区在维护,采用 GPL 授权许可.开发这个分支的原因之一是： 甲骨文公司收购了 MySQL 后,有将 MySQL 闭源的潜在风险,因此社区采用分支的方式来避开这个风险. MariaDB的目的是完全兼容 MySQL,包括 API 和命令行,使之能轻松成为 MySQL 的代替品.

apt install -y libmariadbclient-dev libssl-dev
