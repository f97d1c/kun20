#!/bin/bash --login

eval "export $(jq -r '.["shell变量"]' $1)"

store_path="$PWD/数据库/Oracle/客户端/-安装包"
mkdir -p $store_path

echo "下载安装包"
# wget -t $(jq -r '.["全局参数"]["重试次数"]' $1) -P $store_path -N $package_url

last_file=$(ls -t $store_path/*.zip | head -1)
file_name_full=${last_file##*/}
file_name=${file_name_full%%.*}

unzip -d $store_path -n $last_file

sudo mkdir -p /opt/oracle

sudo cp -rf -n "$store_path/$file_name" /opt/oracle/$file_name

sudo ln -s "/opt/oracle/$file_name/libclntsh.so.12.1" "/opt/oracle/$file_name/libclntsh.so"

if [[ ! "$(grep "Oracle" ~/.bashrc)" ]]; then
tee -a ~/.bashrc <<-EOF

# === Oracle相关追加内容开始 ===

export LD_LIBRARY_PATH=/opt/oracle/$file_name 
export NLS_LANG=AMERICAN_AMERICA.UTF8 # 汉字显示乱码

# === Oracle相关追加内容结束 ===

EOF
fi

echo '[true, ""]'
